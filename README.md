# matt_does_cmake

matt wants cmake, matt gets cmake

## Installation and Running

```bash
# clone this repo
cd matt_does_cmake/
source setup.sh
mkdir build/
cd build/
cmake ../source
source x86*/setup.sh
matt_executable
```

## What It Do

There are two separate packages in this repository: `BestPackage` and `BesterPackage`. `BesterPackage`
resides *beneath* `BestPackage` and compiles down to a library object `BestestLib` that 
the executable in `BestPackage` then links against.

This package relies on setting up the ATLAS top-level CMakeLists.txt (gets built
inside the [setup.sh](setup.sh) script) that is added to the *source/* directory.

```bash
setup.sh
source/
    BestPackage/
        CMakeLists.txt
        BestPackage/
        src/
        util/
            best_hello.cxx
        BesterPackage/
            CMakeLists.txt
            BesterPackage/
                Bestest.h
            src/
                Bestest.cxx
```
